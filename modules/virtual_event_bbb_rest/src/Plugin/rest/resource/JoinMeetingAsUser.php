<?php

namespace Drupal\virtual_event_bbb_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\rest\ModifiedResourceResponse;

/**
 * Provides a resource to get view modes by entity and bundle.
 * @RestResource(
 *   id = "join_meeting_as_user_rest_resource",
 *   label = @Translation("Join Meeting As User Get Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/join-meeting-as-user/{event_id}/{user_id}"
 *   }
 * )
 */
class JoinMeetingAsUser extends ResourceBase {
  /**
   * A current user instance which is logged in the session.
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

 /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $config
   *   A configuration array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A currently logged user instance.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $current_request) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->currentRequest = $current_request;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('virtual_event_bbb_rest'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }
  /**
   * Responds to GET request.
   * Returns a list of taxonomy terms.
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   * Throws exception expected.
   */
  public function get($event_id, $user_id) {

    $user = \Drupal::entityTypeManager()->getStorage('user')->load($user_id);

    // Read Parameter 
    //$event_id = $this->currentRequest->get('event_id');
    
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $event = $virtualEventsCommon->getEventById($event_id);

    if ($event) {
      // Check if meeting is not active,
      // recreate it before showing the join url
      $event = $event->reCreate();

      $entity = $event->getEntity();
      $enabled_event_source = $event->getEnabledSourceKey();
      
      $event_config = $event->getVirtualEventsConfig($enabled_event_source);
      $source_config = $event_config->getSourceConfig($enabled_event_source);
     
      $source_data = $event->getSourceData();
      $eventSourcePlugin = $event->getEventSourcePlugin();
      if(!isset($source_config["data"]["key_type"])) {
        $error_message = $this->t("Couldn't create meeting! please contact system administrator.");      
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);

      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);

      // Check access for current entity, if user can update
      // then we can consider the user as moderator,
      // otherwise we consider the user as normal attendee.
      if ($entity->access('update', $user)) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $user->getDisplayName(), $source_data["settings"]["moderatorPW"]);
        $joinMeetingRole = 'moderator';
      }
      elseif ($entity->access('view', $user)) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $user->getDisplayName(), $source_data["settings"]["attendeePW"]);
        $joinMeetingRole = 'attendee';
      }

      $joinMeetingParams->setRedirect(TRUE);
      $joinMeetingUrl = $bbb->getJoinMeetingURL($joinMeetingParams);     

         
      $join_meeting_as_user['join_meeting_link'] = [
        'meeting_url'  => $joinMeetingUrl,
        'meeting_role' => $joinMeetingRole
      ];
    
    }		

    //$response = new ResourceResponse($join_meeting_as_user);
    $response = new ModifiedResourceResponse($join_meeting_as_user);    
    return $response;      

  }

}
