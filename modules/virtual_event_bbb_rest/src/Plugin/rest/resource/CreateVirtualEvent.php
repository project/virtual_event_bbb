<?php

namespace Drupal\virtual_event_bbb_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\node\NodeInterface;

/**
 * Provides a resource to create virtual event.
 * @RestResource(
 *   id = "create_virtual_event_rest_resource",
 *   label = @Translation("Create Virtual Event Post Rest Resource"),
 *   uri_paths = {
 *     "create" = "/create-virtual-event"
 *   }
 * )
 */
class CreateVirtualEvent extends ResourceBase {
  /**
   * A current user instance which is logged in the session.
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $config
   *   A configuration array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A currently logged user instance.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('virtual_event_bbb_rest'),
      $container->get('current_user')
    );
  }
  /**
   * Responds to POST request.
   * Returns .
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   * Throws exception expected.
   */
  public function post($data) {

    $response_data = [];

    $source_settings_needed_structure = [
      'welcome' => 0,
      'logoutURL' => 0,
      'record' => 0,
      'guest_policy' => 0,
      'muteOnStart' => 0, 
    ];
    
    if (isset($data['settings']) && !empty($data['settings'])) {
      $unsupported_settings = array_diff_key($data['settings'], $source_settings_needed_structure);
      if (isset($unsupported_settings) && !empty($unsupported_settings)) {
        $response_data['error'] = ['settings' => 'Unsupported settings detected!']; 
      }    
    }

    $virtualEventsCommon = \Drupal::service('virtual_events.common');    

    // Validate Parameter entity_id
    if (isset($data['entity_id']) && !empty($data['entity_id'])) {
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $entity = $storage->load($data['entity_id']);
      if (!$entity instanceof NodeInterface) {
        $response_data['error'] = ['entity_id' => 'no entity data available. please check your parameter!']; 
      }
      else {
        if (!$entity->access('update')) {
          $response_data['error'] = ['access_denied' => 'User has no access to create event!'];
        }
      }
    }
    else {
      $response_data['error'] = ['entity_id' => 'This parameter is mandatory!'];
    }

    // Validate Parameter enable_virtual_event
    if (isset($data['enable_virtual_event'])) {
      if (!is_bool($data['enable_virtual_event'])) {
        $response_data['error'] = ['enable_virtual_event' => 'no entity data available. please check your parameter!']; 
      }  
    }
    else {
      $response_data['error'] = ['enable_virtual_event' => 'This parameter is mandatory!'];
    }
  
    // If no error so far we can proceed
    if (!isset($response_data['error'])) {
      $entity_type = $entity->getEntityTypeId();
      $entity_bundle = $entity->bundle();
      $entity_id = $entity->id();
      $eventConfig = $virtualEventsCommon->getVirtualEventsConfig($entity_bundle);
      $event = $virtualEventsCommon->getEventByReference($entity_type, $entity_id);
      $event_id = $entity_bundle . "_" . $entity->uuid();
      $sources = $eventConfig->get("sources");
      $event_name = $eventConfig->get('label') . " " . $entity_id;

      $eventEntityExists = VirtualEventsEventEntity::load($event_id);

      if ($data['enable_virtual_event']) {

        if (!$eventEntityExists) {

          // Save our event config
          $eventEntity = VirtualEventsEventEntity::create([
            "id" => $event_id,
            "label" => $event_name,
            "eventEntityReferenceType" => $entity_type,
            "eventEntityReference" => $entity_id,
            "sources" => [
              "virtual_event_bbb" => [
                "enabled" => 1,
                "settings" => $data['settings'],
              ]
            ]        
          ]);  
          $eventEntity->save();
  
          $response_data['success'] = $eventEntity;
  
        }

      }
      else {

        // Load these entities ($uids) in our case using storage controller.
        // We call loadMultiple method and give $uids array as argument.     
        $itemToDelete = \Drupal::entityTypeManager()->getStorage('virtual_events_event_entity')
          ->load($event_id);
        if ($itemToDelete) {
          $itemToDelete->delete();
          $response_data['success'] = 'Virtual event removed';
        }

      }
    
    } 

    $response = new ResourceResponse($response_data);
    return $response;
    
  }

}
