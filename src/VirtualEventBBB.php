<?php

namespace Drupal\virtual_event_bbb;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Util\UrlBuilder;

/**
 * Defines Virtual Event BBB.
 */
class VirtualEventBBB extends BigBlueButton {

  /**
   * Constructs a new BigBlueButton object.
   */
  public function __construct($securitySecret, $bbbServerBaseUrl) {
    $this->securitySecret   = $securitySecret;
    $this->bbbServerBaseUrl = $bbbServerBaseUrl;
    $this->urlBuilder       = new UrlBuilder($this->securitySecret, $this->bbbServerBaseUrl);
  }

}
