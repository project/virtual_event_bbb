<?php

namespace Drupal\virtual_event_bbb\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Bbbkey plugin plugins.
 */
interface BBBKeyPluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
