<?php

namespace Drupal\virtual_event_bbb\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Bbbkey plugin plugin manager.
 */
class BBBKeyPluginManager extends DefaultPluginManager {


  /**
   * Constructs a new BBBKeyPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BBBKeyPlugin', $namespaces, $module_handler, 'Drupal\virtual_event_bbb\Plugin\BBBKeyPluginInterface', 'Drupal\virtual_event_bbb\Annotation\BBBKeyPlugin');

    $this->alterInfo('virtual_event_bbb_bbbkey_plugin_info');
    $this->setCacheBackend($cache_backend, 'virtual_event_bbb_bbbkey_plugin_plugins');
  }

}
