<?php

namespace Drupal\virtual_event_bbb\Plugin\VirtualEvent\SourcePlugin;

use Drupal\virtual_events\Plugin\VirtualEventSourcePluginBase;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'virtual_event_bbb' soruce.
 *
 * @VirtualEventSourcePlugin(
 *   id = "virtual_event_bbb",
 *   label = @Translation("Virtual Event BBB Source")
 * )
 */
class VirtualEventBBBSource extends VirtualEventSourcePluginBase {

  /**
   * The Plugin Type.
   *
   * @var string
   */
  private $type = "virtual_event_bbb";

  /**
   * Get Plugin Type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function checkMeeting(VirtualEventsEventEntity $event) {
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $event_config = $event->getVirtualEventsConfig($event->id());
    $source_config = $event_config->getSourceConfig($this->pluginId);
    $source_data = $event->getSourceData($this->pluginId);
    if(!isset($source_config["data"]["key_type"])) {
      return;
    }

    $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
    $keys = $keyPlugin->getKeys($source_config);

    $apiUrl = $keys["url"];
    $secretKey = $keys["secretKey"];
    $bbb = new VirtualEventBBB($secretKey, $apiUrl);

    // Check if meeting is not active,
    // recreate it before showing the join url
    $getMeetingInfoParams = new GetMeetingInfoParameters($event->id(), $source_data["settings"]["moderatorPW"]);

    try {
      $response = $bbb->getMeetingInfo($getMeetingInfoParams);
      if ($response->getReturnCode() == 'FAILED') {
        return FALSE;
      }
    }
    catch (\RuntimeException $exception) {
      $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }

    return TRUE;

  }

  /**
   * {@inheritdoc}
   */
  public function createMeeting(VirtualEventsEventEntity $event) {
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $entity = $event->getEntity();
    $entityUrl = \Drupal::request()->getSchemeAndHttpHost() . $entity->toUrl()->toString();
    $logout_url = \Drupal::request()->getSchemeAndHttpHost() . Url::fromRoute('virtual_events.virtual_events_event_ended_controller_reload', ['url' => $entityUrl])->toString();
    $event_config = $event->getVirtualEventsConfig($event->id());

    if ($event) {
      $source_config = $event_config->getSourceConfig($this->pluginId);
      $source_data = $event->getSourceData($this->pluginId);

      if(!isset($source_config["data"]["key_type"])) {
        return;
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);

      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);
      $createMeetingParams = new CreateMeetingParameters($event->id(), $event->label());
      if ($source_data["settings"]["welcome"]) {
        $createMeetingParams->setWelcomeMessage($source_data["settings"]["welcome"]);
      }

      $logoPath = file_create_url(theme_get_setting('logo.url'));
      $createMeetingParams->setLogoutUrl($source_data["settings"]["logoutURL"] ? $source_data["settings"]["logoutURL"] : $logout_url);
      $createMeetingParams->setDuration(0);
      $createMeetingParams->setRecord(TRUE);
      $createMeetingParams->setAllowStartStopRecording(TRUE);
      $createMeetingParams->setLogo($logoPath);

      if ($source_data["settings"]["record"]) {
        $createMeetingParams->setAutoStartRecording(TRUE);
      }

      // Guest Policy      
      if ($source_data["settings"]["guest_policy"]) {
        $guest_policy = $source_data["settings"]["guest_policy"];
        $createMeetingParams->setGuestPolicy($guest_policy);
      }

      try {
        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED') {
          $error_message = $this->t("Couldn't create room! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
        else {
          $typeId = $this->getType();
          $source_data["settings"]["attendeePW"] = $response->getAttendeePassword();
          $source_data["settings"]["moderatorPW"] = $response->getModeratorPassword();
          $event->setSourceData($this->pluginId, $source_data);
          $event->save();
          return $event;
        }
      }
      catch (\RuntimeException $exception) {
        $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      catch (Exception $exception) {
        $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FormStateInterface $form_state, ?array $pluginConfigValues) {

    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $keyPlugins = $BBBKeyPluginManager->getDefinitions();
    $keyOpts = [];
    foreach ($keyPlugins as $key => $plugin_definition) {
      $plugin = $BBBKeyPluginManager->createInstance($key);
      $keyOpts[$key] = $plugin_definition["label"];
    }

    if(!empty($keyOpts)){
      $form["key_type"] = [
        '#title' => t('BBB Key Type'),
        '#type' => 'select',
        '#options' => $keyOpts,
        '#default_value' => isset($pluginConfigValues["key_type"]) ? $pluginConfigValues["key_type"] : array_keys($keyOpts)[0],
        '#attributes' => [
          'id' => 'virtual_event_bbb_key_type',
        ]
      ];
    }
    foreach ($keyPlugins as $key => $plugin_definition) {
      $plugin = $BBBKeyPluginManager->createInstance($key);
      $settings_form = $plugin->buildConfigurationForm($form_state, $pluginConfigValues['keys'][$key]['settings']);
      if(isset($settings_form) && !empty($settings_form)){
        $form['keys'][$key] = [
          '#title' => $plugin->getPluginDefinition()['label'],
          '#description' => isset($plugin_definition["description"]) ? $plugin_definition["description"] : "",
          '#type' => 'details',
          '#open' => TRUE,
          '#weight' => 100,
          '#states' => [
            'visible' => [
              ':input[id="virtual_event_bbb_key_type"]' => ['value' => $key]
            ],
          ],
        ];
        $form['keys'][$key]['settings'] = $plugin->buildConfigurationForm($form_state, $pluginConfigValues['keys'][$key]['settings']);
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntityForm(FormStateInterface $form_state, ?VirtualEventsEventEntity $event, array $source_data = []) {
    $form = [];
    $settings = [];
    if (isset($source_data["settings"])) {
      $settings = $source_data["settings"];
    }

    $form['welcome'] = [
      '#title' => t('Welcome message'),
      '#type' => 'textfield',
      '#default_value' => isset($settings["welcome"]) ? $settings["welcome"] : "",
      '#maxlength' => 255,
      '#description' => t('A welcome message that gets displayed on the chat window when the participant joins. You can include keywords (%%CONFNAME%%, %%DIALNUM%%, %%CONFNUM%%) which will be substituted automatically.'),
      '#disabled' => $event !== NULL,
    ];
    $form['logoutURL'] = [
      '#title' => t('Logout URL'),
      '#type' => 'textfield',
      '#default_value' => isset($settings["logoutURL"]) ? $settings["logoutURL"] : "",
      '#maxlength' => 255,
      '#description' => t('The URL that the users will be redirected to after they logs out of the conference, leave empty to redirect to the current entity.'),
      '#disabled' => $event !== NULL,
    ];
    $form['record'] = [
      '#title' => t('Record meeting'),
      '#type' => 'select',
      '#default_value' => isset($settings["record"]) ? $settings["record"] : "",
      '#options' => [
        0 => t('Do not record'),
        1 => t('Record'),
      ],
      '#description' => t('Whether to automatically start recording when first user joins, Moderators in the session can still pause and restart recording using the UI control.'),
      '#disabled' => $event !== NULL,
    ];

    $form['guest_policy'] = [
      '#title' => t('Guest policy'),
      '#type' => 'select',
      '#default_value' => isset($settings["guest_policy"]) ? $settings["guest_policy"] : "ALWAYS_ACCEPT",
      '#options' => [
        'ALWAYS_ACCEPT' => t('Always accept'),
        'ALWAYS_DENY' => t('Always deny'),
        'ASK_MODERATOR' => t('Ask moderator'),
      ],
      '#description' => t('The guest policy determines whether or not users who send a join request with guest=true will be allowed to join the meeting.'),
      '#disabled' => $event !== NULL,
    ];
    
    return $form;
  }

}
