<?php

namespace Drupal\virtual_event_bbb\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Bbbkey plugin plugins.
 */
abstract class BBBKeyPluginBase extends PluginBase implements BBBKeyPluginInterface {


  // Add common methods and abstract methods for your plugin type here.

}
