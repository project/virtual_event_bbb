<?php

namespace Drupal\virtual_event_bbb\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\JoinMeetingParameters;

/**
 * Defines Virtual Event BBB Link Form.
 */
class VirtualEventBBBLinkForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virtual_event_bbb_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $user = \Drupal::currentUser();
    $args = $form_state->getBuildInfo()["args"];
    $event = $args[0];
    $display_options = $args[1];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $display_options["join_button_text"] ? $display_options["join_button_text"] : $this->t('Join Meeting'),
      '#attributes' => ['class' => ['btn-accent']],
    ];

    $form['event_id'] = [
      '#type' => 'hidden',
      '#default_value' => $event->id(),
    ];

    $form['#attributes'] = ['target' => '_blank'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $user = \Drupal::currentUser();
    $event = $virtualEventsCommon->getEventById($form_state->getValue('event_id'));

    if ($event) {
      // Check if meeting is not active,
      // recreate it before showing the join url
      $event = $event->reCreate();

      $entity = $event->getEntity();
      $enabled_event_source = $event->getEnabledSourceKey();
      $event_config = $event->getVirtualEventsConfig($enabled_event_source);
      $source_config = $event_config->getSourceConfig($enabled_event_source);
      $source_data = $event->getSourceData();
      $eventSourcePlugin = $event->getEventSourcePlugin();
      if(!isset($source_config["data"]["key_type"])) {
        $error_message = $this->t("Couldn't create meeting! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);

      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);

      // Check access for current entity, if user can update
      // then we can consider the user as moderator,
      // otherwise we consider the user as normal attendee.
      if ($entity->access('update')) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $user->getDisplayName(), $source_data["settings"]["moderatorPW"]);
      }
      elseif ($entity->access('view')) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $user->getDisplayName(), $source_data["settings"]["attendeePW"]);
      }

      try {
        $joinMeetingParams->setRedirect(TRUE);

        $url = $bbb->getJoinMeetingURL($joinMeetingParams);
        $form_state->setResponse(new TrustedRedirectResponse($url));

      }
      catch (\RuntimeException $exception) {
        $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      catch (Exception $exception) {
        $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
    }
    else {
      $error_message = $this->t("Couldn't find meeting! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
  }
}
