<?php

namespace Drupal\virtual_event_bbb\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetRecordingsParameters;

/**
 * Defines Virtual Event BBB Recording Controller.
 */
class VirtualEventBBBRecordingController extends ControllerBase {

  /**
   * Viewrecording.
   *
   * @return string
   *   Return Hello string.
   */
  public function viewRecording(VirtualEventsEventEntity $event, $recording_id) {
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $user = \Drupal::currentUser();

    $entity = $event->getEntity();
    $enabled_event_source = $event->getEnabledSourceKey();
    $event_config = $event->getVirtualEventsConfig($enabled_event_source);
    $source_config = $event_config->getSourceConfig($enabled_event_source);
    $source_data = $event->getSourceData();
    $settings = $source_data["settings"];

    if(!isset($source_config["data"]["key_type"])) {
      $error_message = $this->t("Couldn't create meeting! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }

    $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
    $keys = $keyPlugin->getKeys($source_config);

    $apiUrl = $keys["url"];
    $secretKey = $keys["secretKey"];
    $bbb = new VirtualEventBBB($secretKey, $apiUrl);

    $recordingParams = new GetRecordingsParameters();
    $recordingParams->setMeetingID($event->id());
    try {

      $response = $bbb->getRecordings($recordingParams);

      $recordings = [];
      foreach ($response->getRawXml()->recordings->recording as $key => $recording){
        foreach ($recording->playback as $key => $playback){
          foreach ($recording->playback->format as $key => $format){
            if($format->type == "video_mp4" || $format->type == "presentation" || $format->type == "screenshare"){
              $format->recordID = $recording->recordID;
              $recordings[] = $format;
            }
          }
        }
      }

      $recording = "";
      foreach ($recordings as $key => $value) {
        if ($value->recordID == $recording_id) {
          $recording = $value;
          break;
        }
      }

      if ($recording->type == "presentation") {
        return [
          '#theme' => 'virtual_event_bbb_recordings_video_iframe',
          '#url' => $recording->url,
        ];
      }
      else {
        return [
          '#theme' => 'virtual_event_bbb_recordings_video_video',
          '#url' => $recording->url,
        ];
      }
    }
    catch (\RuntimeException $exception) {
      $this->getLogger('virtual_event_bbb')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get recordings! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
  }

}
