<?php

namespace Drupal\virtual_event_bbb\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Bbbkey plugin item annotation object.
 *
 * @see \Drupal\virtual_event_bbb\Plugin\BBBKeyPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class BBBKeyPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
